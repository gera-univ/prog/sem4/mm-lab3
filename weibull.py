import mcg
import math


def weibull_generator(a, b, generator):
    for n in generator:
        yield pow(-math.log(n) / a, 1 / b)


if __name__ == "__main__":
    gen = mcg.mcg(modulus=2 ** 31, a=16807, seed=16807, count=1000)
    weibull_distributed = list(weibull_generator(1, 0.5, gen))
    print(weibull_distributed)
