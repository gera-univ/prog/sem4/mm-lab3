import normal
import math


def lognormal_generator(mu, sigma2, modulus, a, seed, count):
    for n in normal.normal_generator(mu, sigma2, modulus, a, seed, count):
        yield math.exp(n)


if __name__ == "__main__":
    lognormally_distributed = lognormal_generator(mu=0, sigma2=1,
                                                  modulus=2 ** 31, a=16807, seed=16807,
                                                  count=10000)
    print(list(lognormally_distributed))
