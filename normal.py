import mcg
import math


def normal_generator(mu, sigma2, modulus, a, seed, count):
    sequence = list(mcg.mcg(modulus, a, seed, count * 12))
    for i in range(count):
        yield mu + math.sqrt(sigma2) * sum(sequence[12 * i + j] for j in range(12)) - 6


if __name__ == "__main__":
    normally_distributed = normal_generator(mu=0, sigma2=1,
                                            modulus=2 ** 31, a=16807, seed=16807,
                                            count=10000)
    print(list(normally_distributed))
