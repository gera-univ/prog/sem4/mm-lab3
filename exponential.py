import mcg
import math


def exponential_generator(a, generator):
    for n in generator:
        yield -math.log(n) / a


if __name__ == "__main__":
    gen = mcg.mcg(modulus=2 ** 31, a=16807, seed=16807, count=1000)
    exponentially_distributed = list(exponential_generator(2, gen))
    print(exponentially_distributed)